const num1 = +prompt("Please, enter first number");
const num2 = +prompt("Please, enter second number");
const operation = prompt("Please, choose an operation: + , - , * ,  / ");

const add = (num1, num2) => num1 + num2;
const substract = (num1, num2) => num1 - num2;
const multiply = (num1, num2) => num1 * num2;
const divide = (num1, num2) => num1 / num2;

const mathFunc = (num1, num2, func) => {
  return func(num1, num2);
};

if (operation === "+") {
  console.log(mathFunc(num1, num2, add));
} else if (operation === "-") {
  console.log(mathFunc(num1, num2, substract));
} else if (operation === "*") {
  console.log(mathFunc(num1, num2, multiply));
} else if (operation === "/") {
  console.log(mathFunc(num1, num2, divide));
} else {
  alert("Error");
}
